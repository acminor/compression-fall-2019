from PIL import Image, ImageFilter, ImageEnhance, ImageOps
import cv2
import numpy as np
import click
import toml
import matplotlib.pyplot as plt
from pathlib import Path
import skimage.filters as skfilters
import scipy.ndimage as spyimg

# From my previous code using pil and opencv together (Founder Research 2018)
def pilToCV2(img, resize=None):
    if resize and len(resize) == 2:
        img = img.resize(resize)

    img = img.convert('RGB')
    img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)

    return img

# From my previous code using pil and opencv together (Founder Research 2018)
def cv2ToPil(img):
    return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

def parse_pipeline(pipeline):
    global_funcs = list()
    for op in pipeline['operation']:
        print(op)
        if op['type'] == 'gaussian-blur':
            def get_blur(radius):
                return lambda img: img.filter(ImageFilter.GaussianBlur(radius=radius))
            global_funcs.append(get_blur(op['radius']))
        elif op['type'] == 'saturate':
            def get_enhance(amount):
                def enhance(img):
                    enhancer = ImageEnhance.Color(img)
                    return enhancer.enhance(amount)
                return enhance
            global_funcs.append(get_enhance(op['amount']))
        elif op['type'] == 'posterize':
            def get_posterize(bits):
                return lambda img: ImageOps.posterize(img, bits)
            global_funcs.append(get_posterize(op['bits_per_channel']))
        elif op['type'] == 'edge-detect':
            def get_edge_detect(min_threshold, max_threshold, aperture_size, L2gradient):
                def edge_detect(img):
                    img = img.convert(mode='L')
                    img = spyimg.laplace(img)
                    img = Image.fromarray(img)
                    img = img.convert(mode='RGB')
                    return img
                return edge_detect
            global_funcs.append(get_edge_detect(op['min_threshold'], op['max_threshold'],
                                                op['aperture_size'], op['which_l2_flag']))
        else:
            print('ERROR: BAD PIPELINE')
            exit(-1)

    def iapply(global_funcs):
        def int_apply(img):
            for f in global_funcs:
                img = f(img)
            return img
        return int_apply

    return iapply(global_funcs)

@click.command()
@click.argument('images', type=click.Path(exists=True, dir_okay=True))
@click.argument('pipeline', type=click.Path(exists=True, file_okay=True))
def main(images, pipeline):
    file = open(pipeline)
    pipeline = toml.load(file)
    pipeline_func = parse_pipeline(pipeline)

    for img, name in ((Image.open(img), img) for img in Path(images).glob('*')):
        #pipeline_func(img).show()
        plt.imshow(pipeline_func(img))
        plt.title(name)
        plt.show()

main()
